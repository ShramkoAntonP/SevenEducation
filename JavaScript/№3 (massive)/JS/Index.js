//1)"concat"
let number = ["1", "2", "3"];
let letter = ["a", "b", "c"];
let numberLater = number.concat(letter);
    document.write("1)Method concat: " + numberLater);
    document.write("</br>");

//2)"push"
let abc = ["a", "b", "c"];
abc.push("1");
abc.push("2");
abc.push("3");
    document.write("2)Method push: " + abc);
    document.write("</br>");

//3)"reverse"
number.reverse();
    document.write("3)Method reverse: " + number);
    document.write("</br>");

//4)"push"
number.reverse();
number.push("4");
number.push("5");
number.push("6");
    document.write("4)Method push: " + number);
    document.write("</br>");

//5)"unshift"
number.pop("6");
number.pop("5");
number.pop("4");
number.unshift("6");
number.unshift("5");
number.unshift("4");
    document.write("5)Method unshift:" + number);
    document.write("</br>");

//6)Elements call
let frontTools = ["js", "css", "jq"];
    document.write("6)Elements call: " + frontTools[0]);
    document.write("</br>");

//7)"slice"
let numbers = ["1", "2", "3", "4", "5"];
let slice = numbers.slice(0, 3);
    document.write("7)Method slice: " + slice);
    document.write("</br>");

//8)"splice"
numbers.splice(1, 2);
    document.write("8)Method splice: " + numbers);
    document.write("</br>");

//9)"splice"
numbers.splice(1, 0, 2, 10, 3);
    document.write("9)Method splice (add): " + numbers);
    document.write("</br>");

//10)"sort"
let srt = [3, 4, 1, 2, 7];
    srt.sort();
    document.write("10)Method sort: " + srt);
    document.write("</br>");

//11)
let helloWorld = new Array (3);
helloWorld [0] = "Привіт,";
helloWorld [1] = "cвіт";
helloWorld [2] = "!";
helloWorld [1] = "мир";
    document.write("11)Method change element: " + helloWorld[0] + " " + helloWorld[1] + helloWorld[2]);
    document.write("</br>");

//12)
const length = ["Привіт,", "світ", "!"];
    length[0] = "Поки,";
    document.write("12)Method change element: " + length[0] + " " + length[1] + length[2]);
    document.write("</br>");

//13)Creat new massive
var arr = new Array (1, 2, 3, 4, 5);
    document.write("13)a)Method creating massive: " + arr + "; ");
var arr = new Array (5);
    arr[0] = 1;
    arr[1] = 2;
    arr[2] = 3;
    arr[3] = 4;
    arr[4] = 5;
    document.write("b)" + arr);
    document.write("</br>");


//14)n-action massive
let hoa = [
    ["блакитний", "червоний", "зелений"],
    ["blue", "red", "green"],
];
    document.write("14)" + hoa[0][0] + " " + hoa[1][0]);
    document.write("</br>");

//15)
const zxc = ["a", "b", "c", "d"];
    document.write("15)" + zxc[0] + "+" + zxc[1] + ", " + zxc[2]  + "+" + zxc[3]);
    document.write("</br>");

//16)prompt
var question = "Please write how elements we must add?";
var answer = prompt(question);
var mass = [];
    for (let s = 0; s < answer; s++) {
        document.write("16)Cercle 'for': " + new Array(answer));
    }
    document.write("</br>");


//18)"join"
var vegetables = ["Капуста", "Ріпа", "Редиска", "Морква"];
var vegetablesNew = vegetables.join (", ");
document.write("18)Method join: " + vegetablesNew);
document.write("</br>");