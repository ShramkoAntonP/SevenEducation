/*
1. Створити конструктор Animal та розширюючі його конструктори Dog, Cat, Horse.
++Конструктор Animal містить змінні food, location і методи makeNoise, eat, sleep.

++Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
++Dog, Cat, Horse перевизначають методи makeNoise, eat.

++Додайте змінні до конструкторів Dog, Cat, Horse, що характеризують лише цих тварин.

Створіть конструктор Ветеринар, у якому визначте метод який виводить в консоль treatAnimal(Animal animal).

Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.

У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас.
У циклі надсилайте їх на прийом до ветеринара.



Додатково>>>
2. Написати функцію filterBy(), яка прийматиме 2 аргументи.
Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент,
за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null],
і другим аргументом передати 'string', то функція поверне масив [23, null].
*/

function AnimalDoctor() {

}
AnimalDoctor.prototype.treatAnimal = function (animal) {
    console.log(Object.values(animal))
}



function ConstructorAnimal(food, location) {
    this.food = "Water";
    this.location = "Donetsk";
}

Animal.prototype.makeNoise = function () {
    console.log('${this.name} this animal sleep')
}

Animal.prototype.eat = function () {
    console.log('${this.name} this animal eating')
}

Animal.prototype.sleep = function () {
    console.log('Animal sleeping')
}

function ConstructorDog(name, loud, age) {
    this.jack = name;
    this.gavgav = loud;
    this.five = age;
}
ConstructorDog.prototype = new ConstructorDog();

ConstructorDog.prototype.makeNoise = function () {
    console.log('${this.name} this animal sleeping 10 hours')
}
ConstructorDog.prototype.eat = function () {
    console.log('${this.name} this animal eating snack')
}


function ConstructorCat(name, loud, age) {
    this.barsik = name;
    this.myau = loud;
    this.three = age;
}
ConstructorCat.prototype = new ConstructorCat()

ConstructorCat.prototype.makeNoise = function () {
    console.log('${this.name} this animal sleeping never')
}
ConstructorCat.prototype.eat = function () {
    console.log('${this.name} this animal eating fish')
}


function ConstructorHorse(name, loud, age) {
    this.jerebetc = name;
    this.igogo = loud;
    this.nine = age;
}

ConstructorHorse.prototype = new ConstructorHorse()

ConstructorHorse.prototype.makeNoise = function () {
    console.log('${this.name} this animal sleeping on legs')
}
ConstructorHorse.prototype.eat = function () {
    console.log('${this.name} this animal eating greens')
}



ConstructorDog.prototype = new ConstructorAnimal();
ConstructorCat.prototype = new ConstructorAnimal();
ConstructorHorse.prototype = new ConstructorAnimal();

function main() {
    const animal = [new ConstructorDog(), new ConstructorCat(), new ConstructorHorse()];

    animal.forEach((element)=> {
        AnimalDoctor.treatAnimal(element)
    })
}
