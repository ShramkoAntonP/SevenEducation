class MyString {
    constructor(sentence) {
        this.sentence = sentence;
        // this.word1 = first;
        // this.word2 = second;
        // this.word3 = third;
        // this.word4 = fourth;
        // this.sent = [];
    }
    reverse() {
        return this.sentence.split(" ").reverse().join(" ");
    };
    ucFirst() {
        return this.sentence[0].toUpperCase() + this.sentence.slice(1);
    };
    ucWords () {
        return this.sentence.split(" ").map(word => word[0].toUpperCase() + word.substr(1)).join(" ");
    }
}
let sentence = new MyString("i will be developer");

console.log(sentence.reverse());
console.log(sentence.ucFirst());
console.log(sentence.ucWords());