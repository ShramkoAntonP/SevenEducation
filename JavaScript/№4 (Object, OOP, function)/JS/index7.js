class Worker {
    constructor(name, surname, rate, days) {
        this.firstName = name;
        this.lastName = surname;
        this.tax = rate;
        this.workDays = days;
    }
    getSalary() {
        return this.tax * this.workDays;
    };
}

const rabotyaga = new Worker("Anton", "Shramko", 4, 25);

console.log(rabotyaga.getSalary());