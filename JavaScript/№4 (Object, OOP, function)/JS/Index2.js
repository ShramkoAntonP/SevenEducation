function map (fn, array) {
    let newArray = [];
    for (let s = 0; s <array.length; s++) {
        newArray.push(fn(array[s]));
    }
    return newArray;
}
console.log(map(x=>x*3, [2,5,6]));